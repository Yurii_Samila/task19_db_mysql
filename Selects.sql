USE labor_sql;
-- 1 SELECT maker,type FROM product ORDER BY maker;
-- 2 SELECT p.model, l.ram,l.screen,l.price FROM product p, laptop l WHERE l.price > 1000 ORDER BY l.ram ASC,l.price DESC
-- 3 SELECT * FROM printer p WHERE p.color LIKE 'y' ORDER BY p.price DESC 
-- 4 SELECT p.model,pc.speed,pc.hd,pc.cd,pc.price FROM product p,pc pc WHERE pc.cd LIKE '12x' OR '24x' AND pc.price < 600 ORDER BY pc.price DESC 
-- 5 SELECT name, class FROM ships ORDER BY name
-- 6 SELECT speed, price FROM pc WHERE speed >= 500 AND price >= 800 ORDER BY price DESC
-- 7 SELECT * FROM printer WHERE type NOT LIKE 'Matrix' AND price < 300 ORDER BY type DESC
-- 8 SELECT model, speed FROM pc WHERE price BETWEEN 400 AND 600 ORDER BY hd
-- 9 SELECT pc.model, pc.speed, pc.hd, p.maker FROM pc pc,product p WHERE (pc.hd = 10 OR pc.hd = 20) AND p.maker LIKE 'A' ORDER BY pc.speed
-- 10 SELECT pc.model, pc.speed, pc.hd, pc.price,l.screen FROM pc pc, laptop l WHERE l.screen >= 12 ORDER BY price DESC
-- 11 SELECT model, type, price FROM printer WHERE price < 300 ORDER BY type DESC
-- 12 SELECT model, ram, price, screen FROM laptop WHERE ram = 64 ORDER BY screen
-- 13 SELECT model, ram, price, hd FROM pc WHERE ram > 64 ORDER BY hd
-- 14 SELECT model, speed, price, hd FROM pc WHERE speed BETWEEN 500 AND 750 ORDER BY hd
-- 15 SELECT * FROM outcome_o o WHERE o.out > 2000 ORDER BY date DESC
-- 16 SELECT * FROM income_o WHERE inc BETWEEN 5000 AND 10000 ORDER BY inc
-- 17 SELECT * FROM income WHERE point = 1 ORDER BY inc
-- 18 SELECT * FROM outcome o WHERE o.point = 2 ORDER BY o.out
-- 19 SELECT * FROM classes WHERE country LIKE 'Japan' ORDER BY type
-- 20 SELECT name, launched FROM ships WHERE launched BETWEEN 1920 AND 1942 ORDER BY launched DESC
-- 21 SELECT o.*, b.name FROM outcomes o, battles b WHERE b.name LIKE 'Guadalcanal' ORDER BY ship DESC
-- 22 SELECT * FROM outcomes WHERE result LIKE 'sunk' ORDER BY ship DESC
-- 23 SELECT class, displacement, type FROM classes WHERE displacement >=40000 ORDER BY type
-- 24 SELECT trip_no, town_from, town_to, time_out FROM trip WHERE town_from LIKE 'London' OR town_to LIKE 'London' ORDER BY time_out
-- 25 SELECT trip_no, plane, town_from, town_to FROM trip WHERE plane LIKE 'TU-134' ORDER BY time_out DESC
-- 26 SELECT trip_no, plane, town_from, town_to FROM trip WHERE plane NOT LIKE 'IL-86' ORDER BY plane
-- 27 SELECT trip_no, town_from, town_to FROM trip WHERE town_from NOT LIKE 'Rostov' AND town_to NOT LIKE 'Rostov' ORDER BY plane
-- ----------------------------------2---------------------------------
-- 1 SELECT * FROM pc WHERE model LIKE '%1%1%'
-- 2 SELECT * FROM outcome o WHERE o.date LIKE '%03%'
-- 3 SELECT * FROM outcome_o o WHERE o.date LIKE '%14%'
-- 4 SELECT * FROM ships s WHERE s.name RLIKE '^W.*n$'
-- 5 SELECT * FROM ships s WHERE s.name LIKE '%e%e'
-- 6 SELECT * FROM ships WHERE name NOT RLIKE 'a$'
-- 7 SELECT * FROM battles WHERE name RLIKE '.*[ ].*' AND name  NOT RLIKE 'c$'
-- 8 SELECT * FROM trip WHERE time_out BETWEEN '1900-01-01 12:00:00' AND '1900-01-01 17:00:01'
-- 9 SELECT * FROM trip WHERE time_in BETWEEN '1900-01-01 17:00:00' AND '1900-01-01 23:00:01'
-- 10 SELECT * FROM pass_in_trip WHERE place RLIKE '^1.*'
-- 11 SELECT * FROM pass_in_trip WHERE place RLIKE 'c$'
-- 12 SELECT name FROM passenger WHERE name RLIKE '.*[ ]C.*'
-- 13 SELECT name FROM passenger WHERE name NOT RLIKE '.*[ ]J.*'
-- ------------------------------3---------------------------------------
-- 1 SELECT p.maker, p.type, pc.speed, pc.hd FROM product p, pc pc WHERE pc.hd <= 8
-- 2 SELECT p.maker, pc.speed FROM product p, pc WHERE pc.speed >= 600
-- 3 SELECT p.maker, p.type FROM laptop l, product p WHERE p.type LIKE 'Laptop' AND l.speed > 500
-- 4 SELECT l1.model model1, l2.model model2, l1.hd, l1.ram FROM laptop l1, laptop l2 WHERE l1.hd = l2.hd AND l1.ram = l2.ram AND l1.code > l2.code
-- 5 SELECT cl1.country c1, cl2.country c2, cl1.type, cl2.type FROM classes cl1, classes cl2 WHERE cl1.country LIKE cl2.country AND cl1.type NOT LIKE cl2.type
-- 6 SELECT pc.price, p.maker, p.model FROM pc, product p WHERE  pc.price < 600
-- 7 SELECT  p.model, p.maker FROM printer pr, product p WHERE pr.price >= 300 AND p.model = pr.model
-- 8 SELECT pc.price, pc.model, p.maker FROM pc, product p WHERE pc.model = p.model
-- 9 !!!!!!!!!!!!SELECT pc.price, pc.model, p.maker FROM pc, product p WHERE pc.model = p.model
-- 10 SELECT p.* FROM product p, laptop l WHERE p.type LIKE 'Laptop' AND l.speed > 600 AND l.model = p.model
-- 11 SELECT s.name, c.displacement FROM ships s LEFT JOIN classes c ON s.class = c.class
-- 12 SELECT o1.ship, o2.battle FROM outcomes o1, outcomes o2 WHERE o1.result LIKE 'OK' AND o1.ship = o2.ship
-- 13 SELECT s.name, c.country FROM ships s LEFT JOIN classes c ON s.class = c.class
-- 14 SELECT t.trip_no, t.plane, c.name FROM trip t LEFT JOIN company c ON c.ID_comp = t.ID_comp WHERE t.plane LIKE 'Boeing'
-- 15 SELECT p.name, pt.date FROM passenger p LEFT JOIN pass_in_trip pt ON pt.ID_psg = p.ID_psg
-- -------------------------------4--------------------------------------------
-- 1 SELECT  distinct maker FROM Product JOIN PC ON PC.model = Product.model WHERE maker NOT IN(SELECT maker FROM Product JOIN Laptop ON Product.model=Laptop.model );
-- 2 SELECT distinct maker FROM Product JOIN pc ON pc.model = Product.model WHERE maker <> ALL(SELECT maker FROM Product JOIN laptop ON laptop.model = Product.model)
-- 3 SELECT distinct maker FROM Product JOIN pc ON pc.model = Product.model WHERE not maker = ANY(SELECT maker FROM Product JOIN laptop ON laptop.model = Product.model)
-- 4 SELECT maker FROM Product JOIN PC ON PC.model = Product.model WHERE maker IN(SELECT maker FROM Product JOIN Laptop ON Product.model=Laptop.model );
-- 5 SELECT maker FROM product JOIN pc ON pc.model = product.model WHERE maker <> ALL(SELECT maker FROM product JOIN laptop ON laptop.model = product.model)
-- 6 SELECT  distinct maker FROM product JOIN pc ON pc.model = product.model WHERE maker = ANY(SELECT maker FROM product JOIN laptop ON laptop.model = product.model)
-- SELECT * FROM product JOIN pc ON pc.model = product.model
-- SELECT * FROM product WHERE type = 'PC' AND model NOT IN(SELECT model FROM pc);
-- 7 SELECT * FROM product WHERE type = 'PC' AND maker NOT IN(SELECT maker FROM product WHERE type = 'PC' AND model NOT IN(SELECT model FROM pc))
-- 8!!!!!!!!!!!!!!!!!!!!!SELECT country, class, CASE country WHEN 'USA' THEN 'ok' ELSE 'no' END AS country FROM Classes 
-- 9 !SELECT ship FROM Outcomes WHERE result = 'damaged' AND ship IN(SELECT ship FROM Outcomes WHERE result = 'OK')
-- 10 SELECT * FROM PC JOIN Product ON PC.model = Product.model WHERE maker = ANY(SELECT maker FROM Product WHERE maker = 'A')
-- 11 SELECT * FROM Product WHERE type = 'PC' AND NOT model = ANY(SELECT model FROM PC)
-- 12 SELECT * FROM Laptop WHERE price > ALL(SELECT price FROM PC)









